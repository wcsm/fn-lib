# 金融工具箱

#### 介绍

#### 软件架构


#### 安装教程

##### 远程源码安装
```commandline
pip install --upgrade git+git://gitee.com/fsksf/fn-lib.git
```

##### clone到本地源码安装
```commandline
git clone git@gitee.com:fsksf/fn-lib.git
cd fn-lib
pip install .
```

##### pip仓库安装
```commandline
未上传
```

#### 使用说明
##### 基础日历相关
```python
# 引入
from fnlib.options import get_rest_pct, distance, year_trade_days

# 介绍
def get_rest_pct(end_dt, now=None, market='cn'):
    """
    计算到期时间剩余比例
    :param end_dt:
    :param now:
    :param market:
    :return: 剩余时间(精确到小时) / 当年总交易时间
    """


def distance(dt_a, dt_b, market='cn'):
    """
    计算两个交易日之间的交易天数， 两边都包括
    :param dt_a:
    :param dt_b:
    :param market:
    :return:
    """


def year_trade_days(year: int, market='cn'):
    """
    计算某一年的总的交易天数
    :param year:
    :param market:
    :return:
    """
```
##### 标的相关
```python
from fnlib.contract_manager import ContractManager
# 从外部接口获取所有期权标的信息，并整理, 需要有如下这些列
# contract = OptContract(
#     code=row['code'],
#     trading_code=row['trading_code'],
#     name=row['name'],
#     contract_type=row['contract_type'],
#     exchange_code=row['exchange_code'],
#     currency_id=row['currency_id'],
#     underlying_symbol=row['underlying_symbol'],
#     underlying_name=row['underlying_name'],
#     underlying_exchange=row['underlying_exchange'],
#     underlying_type=row['underlying_type'],
#     exercise_price=row['exercise_price'],
#     contract_unit=row['contract_unit'],
#     contract_status=row['contract_status'],
#     list_date=parse(row['list_date']),
#     list_reason=row['list_reason'],
#     list_price=row['list_price'],
#     high_limit=row['high_limit'],
#     low_limit=row['low_limit'],
#     expire_date=parse(row['expire_date']),
#     last_trade_date=row['last_trade_date'],
#     exercise_date=row['exercise_date'],
#     delivery_date=row['delivery_date'],
#     is_adjust=row['is_adjust'],
#     delist_date=row['delist_date'],
#     delist_reason=row['delist_reason'],
# )
cm = ContractManager.opt_contract_from_df(df=df)
# 下面是cm可用的函数定义
def get_underlying_options(underlying_symbol=None):
    """
    根据原始标的查询关联期权
    :param underlying_symbol:
    :return:
    """

def contract_from_df(df: pd.DataFrame):


def opt_contract_from_df(df: pd.DataFrame):
    pass


def get_curr_month_opts(dt: datetime, underlying):
    """
    当月合约, 只包括dt 到月底的合约
    :param dt:
    :param underlying: 期权的原始标的
    :return:
    """


def get_curr_quarter_opts(dt: datetime, underlying):
    """
    当季度合约, 只包括dt 到季度底的合约
    :param dt:
    :param underlying:
    :return:
    """

def get_next_quarter_opts(self, dt: datetime, underlying):
    """
    获取下个季度的期权，dt为当前时间，会自动计算到下个季度
    :param dt:
    :param underlying:
    :return:
    """


def get_next_month_opts(self, dt: datetime, underlying):
    """
    获取下个月的合约
    :param dt:
    :param underlying:
    :return:
    """


def get_curr_month_equal_opts(self, underlying, dt: datetime, price: float):
    """
    当前月的平值合约，只返回dt日期之后的
    :param underlying:
    :param dt:
    :param price:
    :return:
    """


def get_next_month_equal_opts(self, underlying, dt: datetime, price: float):
    """
    下月的平值合约， dt为当前时间
    :param underlying:
    :param dt:
    :param price:
    :return:
    """


def get_curr_quarter_equal_opts(self, underlying, dt: datetime, price: float):
    """
    这个季度的平值合约，dt日期之后的
    :param underlying:
    :param dt:
    :param price:
    :return:
    """

def get_next_quarter_equal_opts(self, underlying, dt: datetime, price: float):
    """
    下一个季度的平值合约， dt为当前时间
    :param underlying:
    :param dt:
    :param price:
    :return:
    """

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


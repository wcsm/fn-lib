import os
from typing import Dict
from functools import lru_cache
from datetime import datetime, timedelta, time, date
from dateutil.relativedelta import relativedelta
from fnlib.utils import parse_datetime, parse_date

dir_path = os.path.dirname(__file__)


class Calender:

    def __init__(self, dt_list, market='cn'):
        self.market = market
        self.calender_dict: Dict[date, int] = {x: count for count, x in enumerate(dt_list)}
        self._month_expire_date: Dict[str, datetime] = {}
        self.init()

    def init(self):
        month = 0
        for dt in self.calender_dict.keys():
            if dt.day > 21 and dt.weekday() == 2 and month != dt.month:
                self._month_expire_date[dt.strftime('%Y%m')] = dt
                month = dt.month

    def expire_date_of_month(self, dt: date):
        key = dt.strftime('%Y%m')
        return self._month_expire_date[key]

    def distance(self, dt_a, dt_b):
        """

        :param dt_a:
        :param dt_b:
        :return:
        """

        pos_a = self.pos(dt_a, direction=1)
        pos_b = self.pos(dt_b, direction=-1)
        if pos_a is not None and pos_b is not None:
            return pos_b - pos_a + 1
        else:
            return None

    @lru_cache()
    def year_trade_days(self, year: int):
        count = 0
        for dt in self.calender_dict.keys():
            if dt.year == year:
                count += 1
        return count

    def pos(self, dt, direction=1):
        dt = parse_date(dt)
        p_int = self.calender_dict.get(dt)
        if p_int is None:
            if direction == 1:
                return self.pos(dt + timedelta(days=1), direction=direction)
            else:

                return self.pos(dt=dt - timedelta(days=1), direction=direction)
        return p_int

    def day_part(self, tm):

        if self.market == 'cn':
            return self._day_part_cn(tm)

    def _day_part_cn(self, tm):
        if tm < time(hour=10, minute=30):
            out = 0
        elif tm < time(hour=13, minute=0):
            out = 0.25
        elif tm < time(hour=14):
            out = 0.5
        elif tm <= time(hour=15):
            out = 0.75
        else:
            out = 1
        return out

    @classmethod
    def from_file(cls, market='cn'):
        with open(os.path.join(dir_path, f'calender_{market}.txt'), 'r') as f:
            calender_data = f.read().split('\n')

        calender_list = [datetime.strptime(x, '%Y%m%d').date() for x in calender_data]
    
        return cls(calender_list)


calender_manager = {
    'cn': Calender.from_file('cn')
}
# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/4 13:54
"""
from collections import defaultdict
from typing import List, Dict, Tuple
from datetime import datetime, timedelta, date
import pandas as pd

from dateutil.relativedelta import relativedelta
from fnlib.obj import OptContract, Contract, Period
from fnlib.utils import get_quarter, get_quarter_start_dt, parse_date, month_str_of_dt
from fnlib.cld import calender_manager


class ContractManager:

    def __init__(self):

        self._contracts: Dict[str, Contract] = {}
        self._option_contracts: Dict[str, OptContract] = {}
        self._underlying_option_map: Dict[str, List[OptContract]] = defaultdict(list)    # 时间从小到大排序
        self._month_opt_contract_map: Dict[str, Dict[str, List[OptContract]]] = \
            defaultdict(lambda: defaultdict(list))   # '202201' --> list()
        self._calender = calender_manager['cn']

    def get_option_contract(self, code):
        return self._option_contracts.get(code)

    def get_contract_period(self, contract: OptContract, dt):
        if contract in self.get_curr_month_opts(underlying=contract.underlying_symbol,
                                                dt=dt,
                                                days_adv_change=0):
            return Period.curr_month
        elif contract in self.get_next_month_opts(underlying=contract.underlying_symbol,
                                                  dt=dt,
                                                  days_adv_change=0):
            return Period.next_month
        elif contract in self.get_curr_quarter_opts(underlying=contract.underlying_symbol,
                                                    dt=dt
                                                    ):
            return Period.curr_quarter
        elif contract in self.get_next_quarter_opts(underlying=contract.underlying_symbol,
                                                    dt=dt):
            return Period.next_quarter
        return None

    def clear_options(self):
        self._option_contracts: Dict[str, OptContract] = {}
        self._underlying_option_map: Dict[str, List[OptContract]] = defaultdict(list)    # 时间从小到大排序
        self._month_opt_contract_map: Dict[str, Dict[str, List[OptContract]]] = \
            defaultdict(lambda: defaultdict(list))   # '202201' --> list()

    def get_underlying_options(self, underlying_symbol=None):
        """
        根据原始标的查询关联期权
        :param underlying_symbol:
        :return:
        """
        if underlying_symbol is None:
            return self._underlying_option_map
        else:
            return self._underlying_option_map[underlying_symbol]

    def contract_from_df(self, df: pd.DataFrame):
        self._contracts = {}
        for _, row in df.iterrows():
            contract = Contract(
                code=row['code'],
                display_name=row['display_name'],
                name=row['name'],
                start_date=row['start_date'],
                end_date=row['end_date'],
                type=row['type']
            )
            self._contracts[contract.code] = contract

    def opt_contract_from_df(self, df: pd.DataFrame):
        self.clear_options()
        df['code'] = df['code'].str.replace('.XSHG', '.SSE')
        for _, row in df.iterrows():
            e_date = parse_date(row['expire_date'])
            contract = OptContract(
                code=row['code'],
                trading_code=row['trading_code'],
                name=row['name'],
                contract_type=row['contract_type'],
                exchange_code=row['exchange_code'],
                currency_id=row['currency_id'],
                underlying_symbol=row['underlying_symbol'],
                underlying_name=row['underlying_name'],
                underlying_exchange=row['underlying_exchange'],
                underlying_type=row['underlying_type'],
                exercise_price=row['exercise_price'],
                contract_unit=row['contract_unit'],
                contract_status=row['contract_status'],
                list_date=parse_date(row['list_date']),
                list_reason=row['list_reason'],
                list_price=row['list_price'],
                high_limit=row['high_limit'],
                low_limit=row['low_limit'],
                expire_date=e_date,
                last_trade_date=parse_date(row['last_trade_date']),
                exercise_date=parse_date(row['exercise_date']),
                delivery_date=parse_date(row['delivery_date']),
                is_adjust=row['is_adjust'],
                delist_date=parse_date(row['delist_date']),
                delist_reason=row['delist_reason'],
            )
            self._option_contracts[contract.code] = contract
            self._underlying_option_map[contract.underlying_symbol].append(contract)
            self._month_opt_contract_map[contract.underlying_symbol][month_str_of_dt(e_date)].append(contract)
        self._sort_options()

    def get_curr_month_opts(self, dt: date, underlying, days_adv_change=0, shift=True):
        """
        当月合约, 只包括dt 到月底的合约
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param underlying: 期权的原始标的
        :param days_adv_change: 设置提前几天切换
        :param shift: 是否开启切换当月到下个月
        :return:
        """
        dt = parse_date(dt)
        month_str = month_str_of_dt(dt)
        expire_date = self._calender.expire_date_of_month(dt)
        if expire_date.day - dt.day < int(days_adv_change):
            if shift:
                dt = dt.replace(day=1)
                return self.get_next_month_opts(dt=dt, underlying=underlying)
            else:
                return []
        else:
            return self._month_opt_contract_map[underlying][month_str]

    def get_curr_quarter_opts(self, dt: date, underlying):
        """
        当**季月**合约
        期权合约的当季合约是指次月之后的第一个季月合约
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param days_adv_change: 设置提前几天切换
        :return:
        """
        dt = parse_date(dt)
        quarter = get_quarter(dt)
        q_end = quarter * 3

        if q_end - dt.month <= 1:
            return self.get_curr_quarter_opts(dt.replace(month=q_end) + relativedelta(months=1), underlying=underlying)
        dt = dt.replace(month=q_end, day=1)
        cl = self.get_curr_month_opts(dt=dt, underlying=underlying, days_adv_change=0, shift=False)
        return cl

    def get_next_quarter_opts(self, dt: date, underlying):
        """
        获取下个季月的期权，dt为当前时间，会自动计算到下个季度
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param underlying:
        :param days_adv_change: 设置提前几天切换
        :return:
        """
        dt = parse_date(dt)
        dt = dt + relativedelta(months=3)
        return self.get_curr_quarter_opts(dt=dt, underlying=underlying)

    def get_next_month_opts(self, dt: date, underlying, days_adv_change=0):
        """
        获取下个月的合约
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param underlying:
        :param days_adv_change: 设置提前几天切换
        :return:
        """
        dt = parse_date(dt)
        e_date = self._calender.expire_date_of_month(dt)
        if (e_date - dt).days < days_adv_change:
            dt = dt + relativedelta(months=2)
        else:
            dt = dt + relativedelta(months=1)
        dt = dt.replace(day=1)
        return self.get_curr_month_opts(dt=dt, underlying=underlying)

    def get_opts(self, dt, underlying, days_adv_change=0):
        c_m = self.get_curr_month_opts(dt=dt, underlying=underlying, days_adv_change=days_adv_change)
        n_m = self.get_next_month_opts(dt=dt, underlying=underlying, days_adv_change=days_adv_change)
        c_q = self.get_curr_quarter_opts(dt=dt, underlying=underlying)
        n_q = self.get_next_quarter_opts(dt=dt, underlying=underlying)
        return c_m + n_m + c_q + n_q

    def get_curr_month_equal_opts(self, underlying,
                                  dt: datetime, price: float,
                                  days_adv_change: int = 0) -> Tuple[OptContract, OptContract]:
        """
        当前月的平值合约
        :param underlying:
        :param dt:
        :param price:
        :param days_adv_change: 设置提前几天切换
        :return:  [call, put]
        """
        opts = self.get_curr_month_opts(dt, underlying=underlying, days_adv_change=days_adv_change)

        return self.get_eq_opts(opts, price)

    @staticmethod
    def get_eq_opts(opts, price):
        pre_opt = {
            'CO': None,
            'PO': None
        }
        pre_dis = {
            'CO': 10000000000000000000000000,
            'PO': 10000000000000000000000000
        }
        ok = {
            'CO': False,
            'PO': False
        }
        for opt in sorted(opts, key=lambda x: x.exercise_price, reverse=True):
            dis = opt.exercise_price - price
            abs_dis = abs(dis)
            cp = opt.contract_type
            if abs_dis <= pre_dis[cp]:
                pre_dis[cp] = abs_dis
                pre_opt[cp] = opt
            elif abs_dis > pre_dis[cp]:
                ok[cp] = True
                if all(ok.values()):
                    break
        return pre_opt['CO'], pre_opt['PO']

    def get_next_month_equal_opts(self, underlying, dt: date, price: float, days_adv_change=0):
        """
        下月的平值合约， dt为当前时间
        :param underlying:
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param price:
        :param days_adv_change: 设置提前几天切换
        :return:
        """
        dt = parse_date(dt)
        e_date = self._calender.expire_date_of_month(dt)
        if (e_date - dt).days < days_adv_change:
            dt = dt + relativedelta(months=2)
        else:
            dt = dt + relativedelta(months=1)
        dt = dt.replace(day=1)
        return self.get_curr_month_equal_opts(underlying=underlying, dt=dt, price=price)

    def get_curr_quarter_equal_opts(self, underlying, dt: date, price: float):
        """
        这个 季月 的平直合约，dt日期之后的
        :param underlying:
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param price:
        :param days_adv_change: 设置提前几天切换
        :return:
        """
        dt = parse_date(dt)
        opts = self.get_curr_quarter_opts(dt, underlying=underlying)
        return self.get_eq_opts(opts, price)

    def get_next_quarter_equal_opts(self, underlying, dt: datetime, price: float):
        """
        下一个 季 月 的平值合约， dt为当前时间
        :param underlying:
        :param dt:  "2022-01-01" 字符串或者 date、datetime格式
        :param price:
        :param days_adv_change: 设置提前几天切换
        :return:
        """
        dt = parse_date(dt)
        dt = dt + relativedelta(months=3)
        dt = get_quarter_start_dt(dt)
        return self.get_curr_quarter_equal_opts(underlying=underlying, dt=dt, price=price)

    def arrange(self):
        self._sort_options()

    def _sort_options(self):
        for cl_all in self._month_opt_contract_map.values():
            for month_cl in cl_all.values():
                month_cl.sort(key=lambda x: x.expire_date)

    def get_equal_opts(self, underlying, dt: datetime,
                       price: float,
                       period: Period = Period.curr_month,
                       days_adv_change: int = 0) -> Tuple[OptContract, OptContract]:
        print(underlying,
              price,
              period,
              days_adv_change)
        if period == Period.curr_month:
            return self.get_curr_month_equal_opts(underlying=underlying,
                                                  dt=dt,
                                                  price=price,
                                                  days_adv_change=days_adv_change)
        elif period == Period.next_month:
            return self.get_next_month_equal_opts(underlying=underlying,
                                                  dt=dt,
                                                  price=price,
                                                  days_adv_change=days_adv_change
                                                  )
        elif period == Period.curr_quarter:
            return self.get_curr_quarter_equal_opts(underlying=underlying,
                                                    dt=dt,
                                                    price=price)
        elif period == Period.next_quarter:
            return self.get_next_quarter_equal_opts(underlying=underlying,
                                                    dt=dt,
                                                    price=price)
        return None, None


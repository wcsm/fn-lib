# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/4 13:55
"""
import enum
from datetime import datetime
from dataclasses import dataclass
import decimal


@dataclass
class Contract:
    code: str
    display_name: str  # 中文名称
    name: str  # 缩写简称
    start_date: str  # 上市日期
    end_date: str  # 退市日期，如果没有退市则为2200-01-01
    type: str  # 类型，stock(股票)，index(指数)，etf(ETF基金)，fja（分级A），fjb（分级B），fjm（分级母基金），
    # mmf（场内交易的货币基金）open_fund（开放式基金）, bond_fund（债券基金）, stock_fund（股票型基金）,
    # QDII_fund（QDII 基金）, money_market_fund（场外交易的货币基金）, mixture_fund（混合型基金）, options(期权)


@dataclass
class OptContract:
    code: str  # 合约代码	10001313.XSHG；CU1901C46000.XSGE；SR903C4700.XZCE；M1707-C-2400.XDCE	注意合约代码使用大写字母
    trading_code: int  # 合约交易代码	510050C1810M02800	合约调整会产生新的交易代码
    name: str  # 合约简称	50ETF购10月2800豆粕购7月2400	合约调整会产生新的合约简称
    contract_type: str  # 合约类型。CO-认购期权，PO-认沽期权	CO
    exchange_code: str  # 证券市场编码，XSHG：上海证券交易所；XSGE：上海期货交易所；XZCE：郑州商品交易所；XDCE：大连商品交易所	XSHG
    currency_id: str  # 货币代码CNY-人民币	CNY
    underlying_symbol: str  # 标的代码	510050.XSHG
    underlying_name: str  # 标的简称	华夏上证50ETF
    underlying_exchange: str  # 标的交易市场	XSHG
    underlying_type: str  # 标的品种类别。ETF-交易型开放式指数基金FUTURE-期货	ETF
    exercise_price: float  # 行权价格	2.8	合约调整会产生新的行权价格
    contract_unit: int  # 合约单位	10000	合约调整会产生新的合约单位
    contract_status: str  # 合约状态：LIST-上市、DELIST-退市。SUSPEND-停牌	DELIST
    # 新期权上市由交易所公布LIST：挂牌日期<=当前日期<=最后交易日DELIST：当前日期>最后交易日
    list_date: datetime  # 挂牌日期	2018-09-25
    list_reason: str  # 合约挂牌原因
    list_price: decimal  # (20,4)	开盘参考价		合约挂牌当天交易所会公布
    high_limit: decimal  # (20,4)	挂牌涨停价		合约挂牌当天交易所会公布
    low_limit: decimal  # (20,4)	挂牌跌停价		合约上市当天交易所会公布
    expire_date: datetime  # 到期日	2018/10/24
    last_trade_date: datetime  # 最后交易日	2018/10/24
    exercise_date: datetime  # 行权日	2018/10/24	50ETF，铜期权是欧式期权，行权日固定。
    # 白糖期权和豆粕期权是美式期权，到期日之前都可以行权，行权日不固定，可为空。
    delivery_date: str  # 交收日期	2018/10/25
    is_adjust: int  # 是否调整		原合约调整为新的合约会发生合约资料的变化1-是，0-否
    delist_date: str  # 摘牌日期	2018/10/24	通联数据和交易所有出入,交易所公布的是2018/10/25摘牌日期=最后交易日T+1
    delist_reason: str  # 合约摘牌原因


class Period(enum.Enum):
    curr_month = 1                  # 当月
    next_month = 2                  # 下月
    curr_quarter = 3                # 当季
    next_quarter = 4                # 下季
import math
from datetime import datetime
from fnlib.cld import calender_manager
from fnlib.utils import parse_datetime


def get_rest_pct(end_dt, now=None, market='cn'):
    """
    计算到期时间剩余比例
    :param end_dt: 合约到期时间
    :param now: 当前时间
    :param market:
    :return: 剩余时间(精确到小时) / 当年总交易时间
    """
    if now is None:
        now = datetime.now()
    end_dt = parse_datetime(end_dt)
    now = parse_datetime(now)
    cal = calender_manager.get(market)
    year_days = cal.year_trade_days(now.year)
    rest_days = cal.distance(now, end_dt)

    hour_part = cal.day_part(now.time())
    return (rest_days - hour_part) / year_days


def distance(dt_a, dt_b, market='cn'):
    """
    计算两个交易日之间的交易天数， 两边都包括
    :param dt_a:
    :param dt_b:
    :param market:
    :return:
    """
    cal = calender_manager.get(market)
    return cal.distance(dt_a=dt_a, dt_b=dt_b)


def year_trade_days(year: int, market='cn'):
    """
    计算某一年的总的交易天数
    :param year:
    :param market:
    :return:
    """
    cal = calender_manager.get(market)
    return cal.year_trade_days(year)


def opt_compose_price(c_price, p_price, r, rest_pct, exercise_price):
    """
    计算合成期货的价格
    :param c_price: 平值合约call的最新价
    :param p_price: 平值合约put的最新价
    :param r: 无风险利率
    :param rest_pct: 剩余时间比例
    :param exercise_price: 行权价
    :return:
    """
    return (c_price - p_price) * math.exp(r * rest_pct) + exercise_price


if __name__ == '__main__':
    print(distance(dt_a=datetime(year=2022, month=3, day=4, hour=12, minute=12),
                   dt_b=datetime(year=2022, month=3, day=7, hour=12, minute=12)))
    print(year_trade_days(2022))
    print(get_rest_pct(now=datetime(year=2022, month=3, day=4, hour=12, minute=12),
                       end_dt=datetime(year=2022, month=3, day=7, hour=12, minute=12)))
    print(get_rest_pct(now=datetime(year=2022, month=3, day=4, hour=14, minute=12),
                       end_dt=datetime(year=2022, month=3, day=7, hour=12, minute=12)))
# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/4 15:29
"""

from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse


def get_quarter(dt: date):
    # 计算所在季度
    return (dt.month - 1) // 3 + 1


def get_quarter_start_dt(dt: date):
    # 计算所在季度开始时间
    q = get_quarter(dt)
    q_first_month = (q-1) * 3 + 1
    return dt.replace(month=q_first_month, day=1)


def parse_date(dt_str):
    """
    返回date类型
    :param dt_str:
    :return:
    """
    if isinstance(dt_str, datetime):
        return dt_str.date()
    elif isinstance(dt_str, date):
        return dt_str
    try:
        return parse(dt_str).date()
    except TypeError:
        return dt_str


def parse_datetime(dt_str):
    """
    返回datetime类型
    :param dt_str:
    :return:
    """
    if isinstance(dt_str, datetime):
        return dt_str
    elif isinstance(dt_str, date):
        return datetime.combine(dt_str, datetime.min.time())
    try:
        return parse(dt_str)
    except TypeError:
        return dt_str


def month_str_of_dt(dt: date):
    return dt.strftime('%Y%m')
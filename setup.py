# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/7 8:59
"""
from setuptools import setup, find_packages

setup(
    name='fnlib',  # Required
    version='0.1.1',  # Required
    description='',  # Required
    long_description='',  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)
    author='fsksf',  # Optional
    author_email='kangyuqiang123@qq.com',  # Optional
    classifiers=[  # Optional
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',
        'License :: Other/Proprietary License',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    keywords='options',  # Optional
    package_data={
        '': ['*.txt', ]
    },
    packages=find_packages(where='.', exclude=['tests', 'test'])
)
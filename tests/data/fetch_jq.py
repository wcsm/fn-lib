# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/4 15:52
"""

from datetime import datetime
import pandas as pd
from jqdatasdk import (
    get_all_securities, get_security_info, get_price, auth, logout, get_trade_days,
    opt, query
)
from sqlalchemy import desc
import contextlib


@contextlib.contextmanager
def auth_login():
    auth('13691086602', 'Qiang.152')
    yield
    logout()

# 初始化直接登录
auth('13691086602', 'Qiang.152')
# 获取标的
option_contract_df = get_all_securities(types=['options', ])
print(option_contract_df)

# 获取 期货的 详细信息  https://www.joinquant.com/help/api/help#JQData:%E6%9C%9F%E6%9D%83%E6%A6%82%E5%86%B5
df: pd.DataFrame = opt.run_query(query(opt.OPT_CONTRACT_INFO).filter(
    opt.OPT_CONTRACT_INFO.expire_date >= datetime(year=2021, month=10, day=1),
    opt.OPT_CONTRACT_INFO.expire_date <= datetime(year=2022, month=12, day=1),
    opt.OPT_CONTRACT_INFO.underlying_symbol.in_(['510300.XSHG', 'SC2205.XINE',
                                                 '510050.XSHG', '000300.XSHG',
                                                 'AU2205.XSGE', 'I2211.XDCE',
                                                 ]),
).order_by(desc(opt.OPT_CONTRACT_INFO.id)))
df.to_csv('options_cn.csv')
#                         display_name               name start_date   end_date     type
# 10000001.XSHG          10000001.XSHG      10000001.XSHG 2015-02-09 2015-03-25  options
# 10000002.XSHG          10000002.XSHG      10000002.XSHG 2015-02-09 2015-03-25  options
# 10000003.XSHG          10000003.XSHG      10000003.XSHG 2015-02-09 2015-03-25  options
# 10000004.XSHG          10000004.XSHG      10000004.XSHG 2015-02-09 2015-03-25  options
# 10000005.XSHG          10000005.XSHG      10000005.XSHG 2015-02-09 2015-03-25  options
# ...                              ...                ...        ...        ...      ...
# ZN2206P27000.XSGE  ZN2206P27000.XSGE  ZN2206P27000.XSGE 2022-02-08 2022-05-25  options
# ZN2206P27500.XSGE  ZN2206P27500.XSGE  ZN2206P27500.XSGE 2022-02-08 2022-05-25  options
# ZN2206P28000.XSGE  ZN2206P28000.XSGE  ZN2206P28000.XSGE 2022-02-08 2022-05-25  options
# ZN2206P28500.XSGE  ZN2206P28500.XSGE  ZN2206P28500.XSGE 2022-02-08 2022-05-25  options
# ZN2206P29000.XSGE  ZN2206P29000.XSGE  ZN2206P29000.XSGE 2022-02-11 2022-05-25  options
# end_dt = datetime.now()
# price_df = get_price(['10000001.XSHG', 'ZN2206P27000.XSGE'], panel=False, count=10, end_date=end_dt)

# print(price_df)

#          time               code    open   close    high     low  volume  money
# 0  2022-02-16      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 1  2022-02-17      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 2  2022-02-18      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 3  2022-02-21      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 4  2022-02-22      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 5  2022-02-23      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 6  2022-02-24      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 7  2022-02-25      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 8  2022-02-28      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 9  2022-03-01      10000001.XSHG     NaN     NaN     NaN     NaN     NaN    NaN
# 10 2022-02-16  ZN2206P27000.XSGE  2378.0  2378.0  2378.0  2378.0     0.0    0.0
# 11 2022-02-17  ZN2206P27000.XSGE  2368.0  2368.0  2368.0  2368.0     0.0    0.0
# 12 2022-02-18  ZN2206P27000.XSGE  2410.0  2410.0  2410.0  2410.0     0.0    0.0
# 13 2022-02-21  ZN2206P27000.XSGE  2252.0  2252.0  2252.0  2252.0     0.0    0.0
# 14 2022-02-22  ZN2206P27000.XSGE  2561.0  2561.0  2561.0  2561.0     0.0    0.0
# 15 2022-02-23  ZN2206P27000.XSGE  2462.0  2462.0  2462.0  2462.0     0.0    0.0
# 16 2022-02-24  ZN2206P27000.XSGE  2220.0  2220.0  2220.0  2220.0     0.0    0.0
# 17 2022-02-25  ZN2206P27000.XSGE  2426.0  2426.0  2426.0  2426.0     0.0    0.0
# 18 2022-02-28  ZN2206P27000.XSGE  2438.0  2438.0  2438.0  2438.0     0.0    0.0
# 19 2022-03-01  ZN2206P27000.XSGE  2364.0  2209.0  2364.0  2364.0     0.0    0.0

# # 退出
# logout()

# # 用数据的时候登录，然后立即退出
# with auth_login():
#     price_df = get_price(['10000001.XSHG', 'ZN2206P27000.XSGE'], panel=False, count=10, end_date=end_dt)
#     print(price_df)


# td = get_trade_days(start_date='2015-01-01', end_date='2023-01-01')
# td_str = [datetime.strftime(t, '%Y%m%d') for t in td]
# with open('calender.txt', mode='w+') as f:
#     f.write('\n'.join(td_str))
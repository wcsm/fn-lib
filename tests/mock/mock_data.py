# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/4 15:56
"""
import os
import pandas as pd
from tests import TESTS_ROOT


class MockData:
    def __init__(self):
        self._options_cn = pd.read_csv(os.path.join(TESTS_ROOT, 'data', 'options_cn.csv'))

    def get_options_basic(self):
        return self._options_cn

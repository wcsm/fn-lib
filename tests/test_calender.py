# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/9 10:29
"""
from fnlib.cld import calender_manager


class TestCalender:

    def setup_method(self):
        self.cn = calender_manager.get('cn')

    def test_distance(self):
        assert self.cn.distance('2022-02-12', '2022-02-20') == 5
        assert self.cn.distance('2022-02-13', '2022-02-15') == 2
        assert self.cn.distance('2022-02-14', '2022-02-16') == 3

    def test_year_trade_days(self):
        assert self.cn.year_trade_days(2022) == 242
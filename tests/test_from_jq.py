# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/8 16:36
"""
from datetime import datetime
from jqdatasdk import auth, opt, get_all_securities, query
from  sqlalchemy import desc
from prettyprinter import pprint
from fnlib.contract_manager import ContractManager
from tests.mock.mock_data import MockData




class TestOptions:

    dt1 = datetime(year=2021, month=11, day=4)
    dt2 = datetime(year=2021, month=11, day=28)

    def setup_method(self):
        self.contract_manager = ContractManager()

        # 初始化直接登录
        auth('13691086602', 'Qiang.152')
        # 获取标的
        option_contract_df = get_all_securities(types=['options', ])
        print(option_contract_df)

        # 获取 期货的 详细信息  https://www.joinquant.com/help/api/help#JQData:%E6%9C%9F%E6%9D%83%E6%A6%82%E5%86%B5
        df: pd.DataFrame = opt.run_query(query(opt.OPT_CONTRACT_INFO).filter(
            opt.OPT_CONTRACT_INFO.expire_date >= datetime(year=2021, month=10, day=1),
            opt.OPT_CONTRACT_INFO.expire_date <= datetime(year=2022, month=12, day=1),
            opt.OPT_CONTRACT_INFO.underlying_symbol.in_(['510300.XSHG', 'SC2205.XINE',
                                                         '510050.XSHG', '000300.XSHG',
                                                         'AU2205.XSGE', 'I2211.XDCE',
                                                         ]),
            ).order_by(desc(opt.OPT_CONTRACT_INFO.id)))
        self.mock_data = MockData()
        self.contract_manager.opt_contract_from_df(df)

    def test_get_curr_month_opts(self):
        assert len(self.contract_manager.get_curr_month_opts(dt=self.dt1, underlying='510050.XSHG')) == 22

        assert len(self.contract_manager.get_curr_month_opts(dt=self.dt2, underlying='510050.XSHG')) == 52

    def test_get_next_month_opts(self):
        c_list = self.contract_manager.get_next_month_opts(dt=self.dt1, underlying='510050.XSHG')
        pprint(c_list)

        assert len(self.contract_manager.get_next_month_opts(dt=self.dt2, underlying='510050.XSHG')) == 52

    def test_get_curr_quarter_opts(self):
        assert len(self.contract_manager.get_curr_quarter_opts(dt=self.dt1, underlying='510050.XSHG')) == 120

        assert len(self.contract_manager.get_curr_quarter_opts(dt=self.dt2, underlying='510050.XSHG')) == 74

    def test_get_next_quarter_opts(self):
        assert len(self.contract_manager.get_next_quarter_opts(dt=self.dt1, underlying='510050.XSHG')) == 120

        assert len(self.contract_manager.get_next_quarter_opts(dt=self.dt2, underlying='510050.XSHG')) == 120

    def test_get_curr_month_equal_opts(self):

        assert self.contract_manager.get_curr_month_equal_opts(dt=self.dt1, underlying='510050.XSHG', price=2.8)

        assert self.contract_manager.get_curr_month_equal_opts(dt=self.dt2, underlying='510050.XSHG', price=2.4)
# -*- coding:utf-8 -*-
"""
@author: fsksf

@since: 2022/3/4 16:02
"""
from prettyprinter import pprint
from fnlib.contract_manager import ContractManager
from tests.mock.mock_data import MockData


class TestOptions:
    dt1 = '2022-03-04'
    dt2 = '2022-02-28'
    dt3 = '2022-01-27'

    def setup_method(self):
        self.contract_manager = ContractManager()
        self.mock_data = MockData()
        self.contract_manager.opt_contract_from_df(self.mock_data.get_options_basic())

    def test_get_curr_month_opts(self):
        assert self.contract_manager.get_curr_month_opts(
            dt='2021-11-04', underlying='510050.XSHG')[0].expire_date.month == 11
        assert self.contract_manager.get_curr_month_opts(
            dt=self.dt2, underlying='510050.XSHG')[0].expire_date.month == 3

    def test_get_next_month_opts(self):
        assert self.contract_manager.get_next_month_opts(
            dt=self.dt1, underlying='510050.XSHG')[0].expire_date.month == 4

        assert self.contract_manager.get_next_month_opts(dt=self.dt2, underlying='510050.XSHG'
                                                         )[0].expire_date.month == 4

    def test_get_curr_quarter_opts(self):
        assert self.contract_manager.get_curr_quarter_opts(dt=self.dt1, underlying='510050.XSHG'
                                                           )[0].expire_date.month == 6
        assert self.contract_manager.get_curr_quarter_opts(dt=self.dt2, underlying='510050.XSHG'
                                                           )[0].expire_date.month == 6

        assert self.contract_manager.get_curr_quarter_opts('2022-01-01', underlying='510050.XSHG'
                                                           )[0].expire_date.month == 3

    def test_get_next_quarter_opts(self):
        assert self.contract_manager.get_next_quarter_opts(dt=self.dt1, underlying='510050.XSHG'
                                                          )[0].expire_date.month == 9

        assert self.contract_manager.get_next_quarter_opts(dt=self.dt2, underlying='510050.XSHG'
                                                           )

    def test_get_curr_month_equal_opts(self):

        assert self.contract_manager.get_curr_month_equal_opts(dt=self.dt1, underlying='510050.XSHG', price=2.8
                                                               )[0]

        assert self.contract_manager.get_curr_month_equal_opts(dt=self.dt2, underlying='510050.XSHG', price=2.4)

    def test_get_curr_month_equal_opts(self):
        tmp = self.contract_manager.get_curr_month_equal_opts(dt=self.dt1, underlying='510050.XSHG', days_adv_change=0,
                                                              price=2.8)
        assert len(tmp) == 2
        assert tmp[0].expire_date.month == tmp[1].expire_date.month == 3
        assert tmp[0].contract_type == 'CO'
        assert tmp[1].contract_type == 'PO'

        tmp = self.contract_manager.get_curr_month_equal_opts(dt=self.dt2, underlying='510050.XSHG', days_adv_change=0,
                                                              price=2.8)
        assert len(tmp) == 2
        assert tmp[0].expire_date.month == tmp[1].expire_date.month == 3
        assert tmp[0].contract_type == 'CO'
        assert tmp[1].contract_type == 'PO'

        tmp = self.contract_manager.get_curr_month_equal_opts(dt=self.dt3, underlying='510050.XSHG', days_adv_change=0,
                                                              price=2.8)
        assert len(tmp) == 2
        assert tmp[0].expire_date.month == tmp[1].expire_date.month == 2
        assert tmp[0].contract_type == 'CO'
        assert tmp[1].contract_type == 'PO'

    def test_get_next_month_equal_opts(self):
        tmp = self.contract_manager.get_next_month_equal_opts(dt=self.dt1, underlying='510050.XSHG',
                                                              price=2.8)
        assert len(tmp) == 2
        assert tmp[0].expire_date.month == tmp[1].expire_date.month == 4
        assert tmp[0].contract_type == 'CO'
        assert tmp[1].contract_type == 'PO'

        tmp = self.contract_manager.get_next_month_equal_opts(dt=self.dt2, underlying='510050.XSHG',
                                                              price=2.8)
        assert len(tmp) == 2
        assert tmp[0].expire_date.month == tmp[1].expire_date.month == 4
        assert tmp[0].contract_type == 'CO'
        assert tmp[1].contract_type == 'PO'

        tmp = self.contract_manager.get_next_month_equal_opts(dt=self.dt3, underlying='510050.XSHG',
                                                              price=2.8)
        assert len(tmp) == 2
        assert tmp[0].expire_date.month == tmp[1].expire_date.month == 3
        assert tmp[0].contract_type == 'CO'
        assert tmp[1].contract_type == 'PO'

